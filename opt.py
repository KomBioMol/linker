import numpy as np
from gromologist import Pdb


class Optimizer:
    def __init__(self, rigid_mol):
        self.flex_mol, self.anchors_flex, self.conv_e, self.coords_flex, self.d0s, self.th0s, \
            self.angles = None, None, None, None, None, None, None
        self.rigid_mol = rigid_mol
        self.anchors_rigid = rigid_mol.anchors
        self.coords_rigid = np.array(rigid_mol.get_coords())
        self.neighlist = []
        self.sigma_heavy = 3.2
        self.sigma_hydrogen = 1.8
        self.eps = 25
        self.eshg = self.eps * (0.5 * (self.sigma_hydrogen + self.sigma_heavy)) ** 12
        self.eshv = self.eps * self.sigma_heavy ** 12
        self.bondheavy_d0 = 1.45
        self.bondhgen_d0 = 1.1
        self.bond_k = 150000
        self.angsp3_th0 = 109
        self.ang_k = 100
        self.dx = 0.0001
        self.a = 0.3
        self.maxiter = 100
        self.sigmas = None
        
    def update_flexible(self, flex_mol, sigma=None):
        self.flex_mol = flex_mol
        self.angles = self.get_angles()
        self.anchors_flex = self.flex_mol.anchors
        self.sigma_heavy = sigma if sigma else self.sigma_heavy
        self.eshg = self.eps * (0.5 * (self.sigma_hydrogen + self.sigma_heavy)) ** 12
        self.eshv = self.eps * self.sigma_heavy ** 12
        self.coords_flex = np.array(self.flex_mol.get_coords())  # TODO update after every step
        self.d0s = np.array([self.bondheavy_d0 - (self.bondheavy_d0 - self.bondhgen_d0) *
                             (bpr[0] not in self.flex_mol.get_heavy() or bpr[1] not in self.flex_mol.get_heavy())
                             for bpr in self.flex_mol.bonds])
        self.th0s = np.array([self.angsp3_th0 for _ in self.angles])
        
    def get_coords(self, atom):
        if atom >= 0:
            return np.array(self.coords_flex[atom])
        else:
            atom = -(atom + 1)
            return np.array(self.coords_rigid[atom])
    
    def update_flex_neighlist(self):
        """
        Called every several opt steps, updates the neighbor list
        over which we calculate repulsive interactions
        :return: None
        """
        self.neighlist = []
        bondset = set(self.flex_mol.bonds)
        for i in range(len(self.flex_mol.atoms)):
            for j in range(i+1, len(self.flex_mol.atoms)):
                xi = np.array(self.get_coords(i))
                xj = np.array(self.get_coords(j))
                if np.linalg.norm(xi - xj) < 1.5 * self.sigma_heavy \
                        and (i, j) not in bondset:  # TODO make self.bonds contain ordered tuples
                    self.neighlist.append((i, j))
        self.update_ext_neighlist()
        self.sigmas = np.array([(self.sigma_heavy - 0.5 * (self.sigma_heavy - self.sigma_hydrogen) *
                                 (npr[0] not in self.flex_mol.get_heavy()
                                  or npr[1] not in self.flex_mol.get_heavy())) ** 12
                                for npr in self.neighlist])  # TODO check
    
    def update_ext_neighlist(self):
        """
        Same as above, but adds pairs for the calculation of
        repulsive interactions with fixed atoms (mostly bases)
        :return: None
        """
        for i in range(len(self.flex_mol.atoms)):
            for j in range(len(self.rigid_mol.atoms)):
                xi = np.array(self.get_coords(i))
                xj = np.array(self.get_coords(-j-1))
                if np.linalg.norm(xi - xj) < 1.5 * self.sigma_heavy:
                    if not (i in self.flex_mol.anchors and j in self.rigid_mol.anchors):
                        self.neighlist.append((i, -j-1))
    
    def get_angles(self):
        """
        Generates a list of angles based on list of bonds
        :return: list of tuples
        """
        angles = []
        for n1, b1 in enumerate(self.flex_mol.bonds):
            for b2 in self.flex_mol.bonds[n1 + 1:]:
                if len(set(b1).intersection(set(b2))) > 0:
                    angles.append(self.b2a(b1, b2))
        return angles
    
    def calc_energy(self):
        ae = self.calc_angles_e()
        re = self.calc_repulsives_e()
        be = self.calc_bonds_e()
        return ae + re + be
    
    def calc_repulsives_e(self):
        dists = np.array([np.linalg.norm(self.get_coords(n[0]) - self.get_coords(n[1])) ** 12 for n in self.neighlist])
        return np.sum(self.eps * self.sigmas / dists)
    
    def calc_bonds_e(self):
        dists = np.array([np.linalg.norm(self.get_coords(n[0]) - self.get_coords(n[1])) for n in self.flex_mol.bonds])
        return np.sum(0.5 * self.bond_k * (dists - self.d0s) ** 2)
    
    def calc_angles_e(self):
        v1s = np.array([self.get_coords(n[1]) - self.get_coords(n[0]) for n in self.angles])
        v2s = np.array([self.get_coords(n[1]) - self.get_coords(n[2]) for n in self.angles])
        angs = np.array([np.min([np.max([-1, x]), 1]) for x in
                         np.sum(v1s * v2s, 1) / (np.linalg.norm(v1s, axis=1) * np.linalg.norm(v2s, axis=1))])
        angles = (180 / np.pi) * np.arccos(angs)
        return np.sum(0.5 * self.ang_k * (angles - self.th0s) ** 2)
        # e = 0
        # for i in self.angles:
        #     e += self.calc_angle_e(*i)
        # return e
    
    def calc_angle_e(self, a1, a2, a3):
        v1 = self.get_coords(a2) - self.get_coords(a1)
        v2 = self.get_coords(a2) - self.get_coords(a3)
        angle = (180 / np.pi) * np.arccos(np.dot(v1, v2) / (np.linalg.norm(v1) * np.linalg.norm(v2)))
        return 0.5 * self.ang_k * (self.angsp3_th0 - angle) ** 2
    
    def calc_grad_an(self):
        grad = np.zeros((len(self.flex_mol.atoms), 3))
        for b in self.flex_mol.bonds:
            grad = self.grad_an_bonds(*b, grad)
        for n in self.neighlist:
            grad = self.grad_an_repulsive(*n, grad)
        for a in self.angles:
            grad = self.grad_an_angles(*a, grad)
        return grad
    
    def grad_an_angles(self, a1, a2, a3, grad):
        th0 = self.angsp3_th0
        v1 = self.get_coords(a1) - self.get_coords(a2)
        v2 = self.get_coords(a3) - self.get_coords(a2)
        v1n = np.linalg.norm(v1)
        v2n = np.linalg.norm(v2)
        dot = np.dot(v1, v2)
        q = dot / (v1n * v2n)
        q = np.min([np.max([-1, q]), 1])
        angle = (180 / np.pi) * np.arccos(q)
        if np.sqrt(1 - q ** 2) * v1n ** 2 * v2n ** 2 == 0:
            return grad
        gr1 = self.ang_k * (angle - th0) * (v2 * v1n * v2n - dot * v1 / v1n) / (
                    np.sqrt(1 - q ** 2) * v1n ** 2 * v2n ** 2)
        gr3 = self.ang_k * (angle - th0) * (v1 * v1n * v2n - dot * v2 / v2n) / (
                    np.sqrt(1 - q ** 2) * v1n ** 2 * v2n ** 2)
        gr2 = -(gr1 + gr3)
        grad[a1, :] -= gr1 * (180 / np.pi)
        grad[a2, :] -= gr2 * (180 / np.pi)
        grad[a3, :] -= gr3 * (180 / np.pi)
        return grad
    
    def grad_an_bonds(self, b1, b2, grad):
        if b1 in self.flex_mol.get_heavy() and b2 in self.flex_mol.get_heavy():
            d0 = self.bondheavy_d0
        else:
            d0 = self.bondhgen_d0
        v = self.get_coords(b2) - self.get_coords(b1)
        d = np.linalg.norm(v)
        gr = self.bond_k * (d - d0) * v / d
        grad[b1, :] -= gr
        grad[b2, :] += gr
        return grad
    
    def grad_an_repulsive(self, n1, n2, grad):
        if n1 in self.flex_mol.get_heavy() and n2 in self.flex_mol.get_heavy():
            es = self.eshv
        else:
            es = self.eshg
        v = self.get_coords(n2) - self.get_coords(n1)
        d = np.linalg.norm(v)
        if d < 0.001:  # protect against interactions between anchors
            return grad
        gr = 12 * (es / (d ** 13)) * v / d
        if n1 >= 0:
            grad[n1, :] += gr
        if n2 >= 0:
            grad[n2, :] -= gr
        return grad
    
    def steep(self):
        """
        The actual optimization subroutine: checks for the
        convergence of energy, updates gradients and the
        learning rate
        :return:
        """
        self.maxiter = 8*len(self.flex_mol.atoms)
        a = self.a
        i = 0
        while i < self.maxiter:
            if i % 5 == 0:
                self.update_flex_neighlist()
            e = np.inf
            grad = self.calc_grad_an()
            for j in self.anchors_flex:
                grad[j] = 0
            grad /= np.linalg.norm(grad)
            # grad[self.dna.fixed] = 0
            self.coords_flex -= grad * a
            new_e = self.calc_energy()
            # print("geo opt iter {}, {} linker atoms, current energy {:.3f}".format(i, len(self.flex_mol.atoms),
            #                                                                       new_e/1000))
            while new_e < e:
                e = new_e
                self.coords_flex -= grad * a
                new_e = self.calc_energy()
            i += 1
        self.flex_mol.set_coords([[x for x in coord] for coord in self.coords_flex])
        # TODO if we're done, we should update self.flex_mol
    
    @staticmethod
    def b2a(b1, b2):
        """
        If two bonds share a common atom, returns an ordered tuple
        of atoms corresponding to a plane angle
        :param b1:
        :param b2:
        :return:
        """
        if b1[0] == b2[0]:
            return b1[1], b1[0], b2[1]
        elif b1[0] == b2[1]:
            return b1[1], b1[0], b2[0]
        elif b1[1] == b2[0]:
            return b1[0], b1[1], b2[1]
        elif b1[1] == b2[1]:
            return b1[0], b1[1], b2[0]
        raise ValueError("{} and {} contain no matching entries".format(b1, b2))
