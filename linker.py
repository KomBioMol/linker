from gromologist import Pdb, Atom
from opt import Optimizer
import numpy as np
from scipy.spatial.transform import Rotation as Rot


class RigidMolecule:
    def __init__(self, filename, anchors):
        self.mol = Pdb(filename)
        self.anchors = anchors
    
    def __getattr__(self, item):
        return self.mol.__getattribute__(item)

        
class Linker:
    def __init__(self, length, mods=None):
        self.mol = Pdb()
        self.mods = mods
        self.anchors = None
        self.length = length
        self.bonds = []
    
    def __getattr__(self, item):
        return self.mol.__getattribute__(item)
    
    def set_anchors(self, rigid_mol):
        a1x, a1y, a1z = rigid_mol.mol.atoms[rigid_mol.anchors[0]].coords()
        a2x, a2y, a2z = rigid_mol.mol.atoms[rigid_mol.anchors[1]].coords()
        atom_template = Atom("ATOM      1  C   LNK A   1       0.000   0.000   0.000  1.00  0.00           C")
        self.insert_atom(0, atom_template, x=a1x, y=a1y, z=a1z)
        self.insert_atom(1, atom_template, x=a2x, y=a2y, z=a2z)
        self.anchors = [0, 1]
        self.bonds = [(0, 1)]
    
    def get_heavy(self):
        return [n for n, a in enumerate(self.atoms) if not a.atomname.startswith('H')]
    
    def add_hydrogens(self):
        """
        when the chain is fully grown, we can add hydrogens & reoptimize
        :return:
        """
        noh_carbons = self.get_noh()
        d_ch = 1.09
        self.bonds = []
        heavy_atoms = [n for n, a in enumerate(self.atoms) if a.atomname in ['N', 'O', 'C']]
        for n in range(1, len(heavy_atoms) - 1):
            heavy_atoms = [n for n, a in enumerate(self.atoms) if a.atomname in ['N', 'O', 'C']]
            self.bonds += [(heavy_atoms[n - 1], heavy_atoms[n])]
            if n in noh_carbons:
                if n in [a[1] for a in self.mods]:
                    self.add_mod(n)
                continue
            a = heavy_atoms[n]
            ai = np.array(self.atoms[heavy_atoms[n-1]].coords())
            aj = np.array(self.atoms[a].coords())
            ak = np.array(self.atoms[heavy_atoms[n+1]].coords())
            v1 = ai - aj
            v2 = ak - aj
            d_cc = np.mean([np.linalg.norm(v1), np.linalg.norm(v2)])
            scaling = d_ch/d_cc
            for h in range(2):
                in_plane = scaling * 0.5 * (v1 + v2)
                cross_prod = np.cross(v1, v2)
                out_of_plane = (-1)**h * scaling * 0.5 * np.linalg.norm(v1-v2) * cross_prod/np.linalg.norm(cross_prod)
                self.insert_atom(a+1, self.atoms[a], atomname="H",
                                 x=self.atoms[a].x + in_plane[0] + out_of_plane[0],
                                 y=self.atoms[a].y + in_plane[1] + out_of_plane[1],
                                 z=self.atoms[a].z + in_plane[2] + out_of_plane[2])
                self.bonds += [(a, a+1+h)]
        self.renumber_all()
        heavy_atoms = [n for n, a in enumerate(self.atoms) if a.atomname in ['N', 'O', 'C']]
        self.bonds += [(heavy_atoms[-2], heavy_atoms[-1])]
        self.anchors[-1] = len(self.atoms) - 1
        
    def add_mod(self, n):
        curr_mod = [x for x in self.mods if x[1] == n][0]
        heavy_atoms = [n for n, a in enumerate(self.atoms) if a.atomname in ['N', 'O', 'C']]
        a = heavy_atoms[n]
        ai = np.array(self.atoms[heavy_atoms[n - 1]].coords())
        aj = np.array(self.atoms[a].coords())
        ak = np.array(self.atoms[heavy_atoms[n + 1]].coords())
        v1 = ai - aj
        v2 = ak - aj
        if curr_mod[0] == 'carbonyl':
            self.add_carbonyl(a, v1, v2)
        elif curr_mod[0] == 'click':
            self.add_click(a, heavy_atoms[n - 1], heavy_atoms[n + 1], v1, v2)
        
    def add_carbonyl(self, a, v1, v2):
        bond_len = 1.5
        vec = v1 + v2
        oxy = -bond_len * vec/np.linalg.norm(vec)
        self.insert_atom(a + 1, self.atoms[a], atomname="OX",
                         x=self.atoms[a].x + oxy[0],
                         y=self.atoms[a].y + oxy[1],
                         z=self.atoms[a].z + oxy[2])
        self.bonds += [(a, a+1)]
        
    def add_click(self, a, previous_a, next_a, v1, v2):
        vecs_n = [v1 + 1.5 * v2, v2 + 1.5 * v1]
        self.atoms[previous_a].atomname = "N"
        for i in range(2):
            self.insert_atom(a + 1, self.atoms[a], atomname="NX",
                             x=self.atoms[a].x + vecs_n[i][0],
                             y=self.atoms[a].y + vecs_n[i][1],
                             z=self.atoms[a].z + vecs_n[i][2])
        self.bonds += [(a+1, a+2)]
        self.bonds += [(previous_a, a+1)]
        self.bonds += [(a+2, next_a+2)]
    
    def get_noh(self):
        if not self.mods:
            return []
        else:
            noh = []
            for mod in self.mods:
                if mod[0] == 'carbonyl':
                    noh.append(mod[1])
                elif mod[0] == 'click':
                    noh.extend([mod[1]-1, mod[1], mod[1]+1])
                else:
                    raise ValueError(mod[0] + " is an unknown keyword")
            return noh
    
    def grow_by_one(self, init_rigid=None):
        """
        when a poly-C chain is relaxed, we add one atom and optimize again
        :return:
        """
        if init_rigid:
            self.find_init_bead(init_rigid)
        else:
            self.interpolate_atoms()
            self.bonds.append((len(self.atoms)-2, len(self.atoms)-1))
        self.renumber_all()
        self.bonds = [(i, i+1) for i in range(len(self.atoms)-1)]
    
    def find_init_bead(self, init_rigid):
        anchors = np.array(self.get_coords())
        other = np.array(rigid.get_coords())
        midpoint = np.mean(anchors, 0)
        vec = anchors[1] - anchors[0]
        vec /= np.linalg.norm(vec)
        rot = Rot.from_rotvec(np.pi/3*vec)
        if self.check_clash(other, midpoint):
            return
        for r in np.linspace(2, 20, 10):
            q = r/(np.sqrt(1+(vec[0]/vec[1])**2))
            test = midpoint + np.array([q, -(vec[0]/vec[1])*q, 0])
            if self.check_clash(other, test):
                return
            for theta in range(5):
                test -= midpoint
                test = rot.apply(test)
                test += midpoint
                if self.check_clash(other, test):
                    return
        raise RuntimeError('could not place the initial bead')
        
    def check_clash(self, rigid_coords, ref_coord, thresh=6):
        a = rigid_coords - ref_coord
        dists = np.sum(a**2, 1)**0.5
        if all([x > thresh for x in dists]):
            self.insert_atom(1, self.atoms[0], x=ref_coord[0], y=ref_coord[1], z=ref_coord[2])
            self.anchors[-1] = len(self.atoms)-1
            return True
        else:
            return False
    
    def interpolate_atoms(self):
        nat = len(self.atoms)
        scaling_factor = (nat-1)/nat
        new_location_fractions = [(scaling_factor*i) - int(scaling_factor*i) for i in range(1, nat)]
        for n in range(nat-1):
            pair = self.bonds[n]
            ref_atom = self.atoms[n+1]
            hook_atom = self.atoms[n]
            frac = new_location_fractions[n]
            old_location = np.array(hook_atom.coords())
            vec = np.array(self.atoms[pair[1]].coords()) - np.array(self.atoms[pair[0]].coords())
            shift = frac * vec
            nloc = old_location + shift
            if n < (nat-2):
                ref_atom.set_coords(nloc)
            else:
                self.insert_atom(nat-1, ref_atom, x=nloc[0], y=nloc[1], z=nloc[2])
                self.anchors[-1] = len(self.atoms) - 1
    

if __name__ == "__main__":
    sys = 'start_1.pdb'
    linker_length = 20
    rigid = RigidMolecule(sys, [550, 1045])
    link = Linker(linker_length, mods=[('carbonyl', 1), ('click', 6)])
    link.set_anchors(rigid)
    link.grow_by_one(rigid)
    opt = Optimizer(rigid)
    opt.update_flexible(link)
    opt.steep()
    #struct = 0
    while len(link.mol.atoms) < linker_length:
        #link.save_pdb('temp{}.pdb'.format(struct))
        #struct += 1
        print("optimizing a {}-carbon linker...".format(len(link.atoms)))
        if len(link.atoms) % 2 == 0:
            opt.steep()  # TODO start with large sigma and decrease gradually
        link.grow_by_one()
        opt.update_flexible(link)
    opt.steep()
    print("adding hydrogens...")
    link.add_hydrogens()
    opt.update_flexible(link)
    opt.steep()
    link.mol.save_pdb()
